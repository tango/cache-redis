module gitea.com/tango/cache-redis

go 1.12

require (
	gitea.com/lunny/tango v0.6.0
	gitea.com/tango/cache v0.0.0-20200330032101-c35235184e65
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/unknwon/com v1.0.1
	gopkg.in/bufio.v1 v1.0.0-20140618132640-567b2bfa514e // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/ini.v1 v1.57.0
	gopkg.in/redis.v2 v2.3.2
)
